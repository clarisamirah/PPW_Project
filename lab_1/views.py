from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Clarisa Mirah Sekarsari' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 2, 14) #TODO Implement this, format (Year, Month, Date)
npm = 1706984530 # TODO Implement this
uni = 'Universitas Indonesia'
hob = 'Browsing, Gambar'
descr = 'Saya adalah mahasiswa Sistem Informasi angkatan 2017. Saya tinggal di Jakarta, tapi saya juga ngekos di Barel.'

mhs_namef = 'Felia Risky Faisal' # TODO Implement this
birth_datef = date(1999, 6, 23) #TODO Implement this, format (Year, Month, Date)
npmf = 1706984581 # TODO Implement this
unif = 'Universitas Indonesia'
hobf = 'Travelling'
descrf = 'Saya adalah mahasiswa Sistem Informasi angkatan 2017. Saat ini saya sedang disibukkan oleh beberapa kepanitiaan. Biasanya saya tinggal di Jakarta, tapi saya sedang ngekos di Barel.'

mhs_nameb = 'Hana Raissya' # TODO Implement this
birth_dateb = date(1999, 5, 27) #TODO Implement this, format (Year, Month, Date)
npmb = 1706979266 # TODO Implement this
unib = 'Universitas Indonesia'
hobb = 'Baca Komik'
descrb = 'Saya adalah mahasiswa Sistem Informasi angkatan 2017. Saya berasal jauh dari Sumatera Barat, saat ini saya tinggal di suatu kosan di Barel'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'uni': uni, 'hobby': hob, 'description' : descr, 'namef': mhs_namef, 'agef': calculate_age(birth_datef.year), 'npmf': npmf, 'unif': unif, 'hobbyf': hobf, 'descriptionf' : descrf, 'nameb': mhs_nameb, 'ageb': calculate_age(birth_dateb.year), 'npmb': npmb, 'unib': unib, 'hobbyb': hobb, 'descriptionb' : descrb}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
