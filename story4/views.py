from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from .forms import formJadwal
from .models import JadwalPribadi
response = {'author' : "Clarisa Mirah"}
# Create your views here.
def index(request) :
	return render(request, 'beranda.html')
def profil(request) :
	return render(request, 'profil.html')
def ketrampilan(request) :
	return render(request, 'ketrampilan.html')
def kontak(request) :
	return render(request, 'kontak.html')
def daftar(request) :
	return render(request, 'daftar.html')
def jadwal(request) :
	form = formJadwal(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['nama_kegiatan'] = request.POST['nama_kegiatan']
		response['hari'] = request.POST['hari']
		response['tanggal'] = request.POST['tanggal']
		response['waktu'] = request.POST['waktu']
		response['tempat'] = request.POST['tempat']
		response['kategori'] = request.POST['kategori']
		sched = JadwalPribadi(nama_kegiatan = response['nama_kegiatan'], hari = response['hari'], tanggal = response['tanggal'], waktu = response['waktu'], tempat = response['tempat'], kategori = response['kategori'])
		sched.save()
		return HttpResponseRedirect('../jadwal')
	else:
		print('not found')
	response['formjadwal'] = form
	return render(request, 'formpage.html', response)
def catatanjadwal(request) :
	response['schedules'] = JadwalPribadi.objects.all()
	return render(request, 'notesched.html', response)
def delete(request):
   JadwalPribadi.objects.all().delete()
   return HttpResponseRedirect('../catatanjadwal')
	