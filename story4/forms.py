from django import forms
class formJadwal(forms.Form) :
	attrs = {
		'class' : 'form-control',
		'style' : 'width: 400px;'
	}
	nama_kegiatan = forms.CharField(label = 'Nama Kegiatan', required = False, max_length = 100, empty_value = 'Nama Kegiatan tidak diketahui', widget = forms.TextInput(attrs=attrs))
	hari = forms.CharField(label = 'Hari', required = False, max_length = '7', empty_value = 'Hari tidak diketahui', widget = forms.TextInput(attrs = attrs))
	tanggal = forms.DateField(label = 'Tanggal', required = False, widget = forms.DateInput(attrs = {'class' : 'form-control', 'type' : 'date', 'style' : 'width: 400px;'}))
	waktu = forms.TimeField(label = 'Waktu', required = False, widget = forms.TimeInput(attrs = {'class' : 'form-control', 'type' : 'time', 'style' : 'width: 400px;'}))
	tempat = forms.CharField(label = 'Tempat', required = False, max_length = 500, empty_value = 'Tempat tidak diketahui', widget = forms.TextInput(attrs=attrs))
	kategori = forms.CharField(label = 'Kategori', required = False, max_length = 50, empty_value = 'Kategori tidak diketahui', widget = forms.TextInput(attrs=attrs))