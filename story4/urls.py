from django.conf.urls import url
from .views import index
from .views import profil
from .views import ketrampilan
from .views import kontak
from .views import daftar
from .views import jadwal
from .views import catatanjadwal
from .views import delete
urlpatterns = [
	url(r'^$', index, name = 'index'),
	url(r'^profil/', profil, name = 'profil'),
	url(r'^ketrampilan/', ketrampilan, name = 'ketrampilan'),
	url(r'^kontak/', kontak, name = 'kontak'),
	url(r'^daftar/', daftar, name = 'daftar'),
	url(r'^jadwal/', jadwal, name = 'jadwal'),
	url(r'^catatanjadwal/', catatanjadwal, name = 'catatanjadwal'),
	url(r'^delete/', delete, name = 'delete'),
]
