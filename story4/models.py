from django.db import models
from django.utils import timezone
from datetime import datetime, date
from datetime import datetime, time
# Create your models here.
class JadwalPribadi(models.Model):
	nama_kegiatan = models.CharField(max_length = 100)
	hari = models.CharField(max_length = 7)
	tanggal = models.DateField()
	waktu = models.TimeField()
	tempat = models.CharField(max_length = 500)
	kategori = models.CharField(max_length = 50)